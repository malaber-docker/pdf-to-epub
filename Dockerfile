FROM ubuntu:20.10

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt install -y calibre

VOLUME ["/pdf"]
WORKDIR /pdf
